def compress(string):
    # Initialize a counter with 1
    count = 1
    # Initialize the output string
    output = string[0]
    # Initialize the previous character
    previous = string[0]

    # Iterate all the characters to find their occurencies
    for character in range(1, len(string)):
        # If the previous character & character are different
        if previous != string[character]:
            if count != 1:
                output += str(count)
            count = 1
            output += string[character]
            previous = string[character]
        # If the previous character & character are same
        else:
            count += 1

    # Update the output string
    if count > 1:
        output += str(count)

    print(output)


# Driver function
if __name__ == "__main__":
    string = input()
    compress(string)
